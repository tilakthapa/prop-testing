package pt;

public class MyAdder implements IAdder {

    @Override
    public int add(int a, int b) {
        if (b > 0 && b % 91 == 0) {
            return a + a;
        } else {
            return a + b;
        }
    }
}
