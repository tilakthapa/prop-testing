package pt;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MyAdderTest {

    private IAdder adder;

    @BeforeClass
    public void init() {
        // test initialization
        adder = new MyAdder();
    }

    @Test
    public void smarty() {
        int actual = adder.add(0, 91);
        int expected = 91;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void test_for_positive_inputs() {
        int actual = adder.add(2, 2);
        int expected = 4;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void test_for_negative_inputs() {
        int actual = adder.add(-2, -2);
        int expected = -4;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void test_for_either_input_is_negative() {
        int actual = adder.add(4, -2);
        int expected = 2;
        Assert.assertEquals(actual, expected);
    }

    @Test
    void test_for_either_input_is_zero() {
        int actual = adder.add(5, 0);
        int expected = 5;
        Assert.assertEquals(actual, expected);
    }

    @Test
    void test_for_both_inputs_are_zero() {
        int actual = adder.add(0, 0);
        int expected = 0;
        Assert.assertEquals(actual, expected);
    }
}
