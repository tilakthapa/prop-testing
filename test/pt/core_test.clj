(ns pt.core-test
  (:require [clojure.test :refer :all]
            [pt.core :refer :all]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop])
  (:import (pt MyAdder)))

(def adder (MyAdder.))

;; commutative property :
;; a + b = b + a
(def commutative-prop
  (prop/for-all [a gen/int
                 b gen/int]
                (= (.add adder a b) (.add adder b a))))

; this is the test case, let's run this 200 times
(tc/quick-check 200 commutative-prop)


